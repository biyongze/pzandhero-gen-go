/*
@Author: nullzz
@Date: 2020/12/11 3:07 下午
@Version: 1.0
@DEC:
*/
package main

import (
	"errors"
	"flag"
	"pzandhero-gen-go/internal/common"
	"pzandhero-gen-go/internal/pb2dbstate"
)

func main() {
	var dir string
	var out string
	var pbPkg string
	flag.StringVar(&dir, "pb", "", "输入路径")
	flag.StringVar(&out, "out", "", "输出路径")
	flag.StringVar(&pbPkg, "pb_pkg", "", "pb pkg路径")

	flag.Parse()
	if dir == "" || out == "" || pbPkg == "" {
		panic(errors.New("参数无效"))
	}
	err := pb2dbstate.ParserDir(dir, out, pbPkg)
	common.CheckErr(err)
}
