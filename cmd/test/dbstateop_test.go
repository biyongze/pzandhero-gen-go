/*
@Author: nullzz
@Date: 2020/12/12 4:58 下午
@Version: 1.0
@DEC:
*/
package test

import (
	"pzandhero-gen-go/internal/common"
	"pzandhero-gen-go/internal/pb2dbstate"
	"testing"
)

func TestDbStatop(t *testing.T) {
	var dir = "/Users/nullzz/pzandhero-gen-go/proto/dbstate"
	var out = "./gen"
	var pbPkg = "server/pkg/gen"
	err := pb2dbstate.ParserDir(dir, out, pbPkg)
	common.CheckErr(err)
}
