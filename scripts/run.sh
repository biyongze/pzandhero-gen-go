#!/bin/bash

work_path=$(dirname "$0")
cd ./"${work_path}" # 当前位置跳到脚本位置

#说明
show_usage="args: [-p , -o , -g]"

#pb路径 -pb
opt_pb=""
#out路径 -out
opt_out=""
#service pb
opt_service=""
opt_pb_pkg=""

while getopts "p:o:g:" OPT; do
  case ${OPT} in
  p)
    opt_pb=${OPTARG}
    ;;
  o)
    opt_out=${OPTARG}
    ;;
  g)
    opt_pb_pkg=${OPTARG}
    ;;
  ?)
    echo "$OPT", "$OPTARG", "$show_usage"
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

if [[ -z $opt_pb || -z $opt_out || -z $opt_pb_pkg ]]; then
  echo "$show_usage"
  echo "opt_pb: $opt_pb , opt_out: $opt_out ,opt_pb_pkg: $opt_pb_pkg"
  exit 0
fi

binPath=""
protcPath=""
if [ "$(uname)" == "Darwin" ]; then
  # Mac OS X 操作系统
  binPath="../bin/osx"
  protcPath="protoc-3.0.0-osx-x86_64"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
  binPath="../bin/linux"
  protcPath="protoc-3.0.0-linux-x86_64"
  # GNU/Linux操作系统
#elif [ "$(expr substr $(uname -s) 1 10)"=="MINGW32_NT" ]; then
# # Windows NT操作系统
fi

topath="$opt_out"/router
if [ ! -d "$topath" ]; then
  mkdir "$topath"
fi

echo "gen router -------"
./${binPath}/${protcPath} -I=. -I="$opt_pb" -I=../proto_include --plugin=protoc-gen-grouter=${binPath}/protoc-gen-grouter --grouter_out="$topath" "$opt_pb"/service/*.proto

echo "gen dbstateop -------$opt_pb,$opt_out,$opt_pb_pkg"
./${binPath}/gen-dbstateop -pb="$opt_pb"/dbstate -out="$opt_out" -pb_pkg="$opt_pb_pkg"
