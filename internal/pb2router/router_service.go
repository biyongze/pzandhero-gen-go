package pb2router

import (
	"bytes"
	"log"
	"path"
	"pzandhero-gen-go/internal/generator"
	"text/template"
)

type ServiceSpec struct {
	ServiceName string
	Methods     []*ServiceMethodSpec
}

type ServiceMethodSpec struct {
	MethodName     string
	InputTypeName  string
	OutputTypeName string
}

type Gen struct {
	ServiceFileName string
	Services        map[string]*ServiceSpec
}

//func HttpPath(m *descriptor.MethodDescriptorProto) (string, error) {
//	ext, err := proto.GetExtension(m.Options, options.E_Http)
//	if err != nil {
//		return "", err
//	}
//	opts, ok := ext.(*options.HttpRule)
//	if !ok {
//		return "", fmt.Errorf("extension is %T; want an HttpRule", ext)
//	}
//
//	switch t := opts.Pattern.(type) {
//	default:
//		return "", nil
//	case *options.HttpRule_Get:
//		return t.Get, nil
//	case *options.HttpRule_Post:
//		return t.Post, nil
//	case *options.HttpRule_Put:
//		return t.Put, nil
//	case *options.HttpRule_Delete:
//		return t.Delete, nil
//	case *options.HttpRule_Patch:
//		return t.Patch, nil
//	case *options.HttpRule_Custom:
//		return t.Custom.Path, nil
//	}
//}

func (p *MyPlugin) genRouterCode(file *generator.FileDescriptor) {
	p.P("// TODO: service code, Name = " + file.GetName())
	filenameall := path.Base(file.GetName())
	filesuffix := path.Ext(file.GetName())
	fileprefix := filenameall[0 : len(filenameall)-len(filesuffix)]
	gen := &Gen{
		ServiceFileName: generator.CamelCase(fileprefix),
		Services:        make(map[string]*ServiceSpec),
	}
	for _, svc := range file.FileDescriptorProto.Service {
		spec := &ServiceSpec{
			ServiceName: generator.CamelCase(svc.GetName()),
		}
		for _, m := range svc.Method {
			spec.Methods = append(spec.Methods, &ServiceMethodSpec{
				MethodName:     generator.CamelCase(m.GetName()),
				InputTypeName:  p.TypeName(p.ObjectNamed(m.GetInputType())),
				OutputTypeName: p.TypeName(p.ObjectNamed(m.GetOutputType())),
			})
		}
		gen.Services[spec.ServiceName] = spec
	}

	var buf bytes.Buffer
	t := template.Must(template.New("").Parse(tmplService))
	err := t.Execute(&buf, gen)
	if err != nil {
		log.Fatal(err)
	}

	p.P(buf.String())
}

func (p *MyPlugin) genImportCode(file *generator.FileDescriptor) {
	//p.P(`import ""`)
	p.P("import (")
	p.P(`"bitbucket.org/funplus/golib/err2"`)
	p.P(`"bitbucket.org/funplus/golib/router/gen/netutils"`)
	p.P(`"bitbucket.org/funplus/golib/zaplog"`)
	p.P(`"github.com/gin-gonic/gin"`)
	p.P(`"server/internal/logic/linkserver"`)
	p.P(`"server/pkg/gen/msg"`)
	p.P("service2", " ", `"server/pkg/gen/service"`)
	p.P(")")
	p.P()

}
