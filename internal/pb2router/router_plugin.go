/*
@Author: nullzz
@Date: 2020/9/30 12:06 下午
@Version: 1.0
*/
package pb2router

import (
	"pzandhero-gen-go/internal/generator"
)

func init() {
	generator.RegisterPlugin(new(MyPlugin))
}

type MyPlugin struct {
	*generator.Generator
}

func (p *MyPlugin) Name() string {
	return "grouter"
}

func (p *MyPlugin) Init(g *generator.Generator) {
	p.Generator = g
}

func (p *MyPlugin) Generate(file *generator.FileDescriptor) {
	p.genRouterCode(file)
}

func (p *MyPlugin) GenerateImports(file *generator.FileDescriptor) {
	if len(file.Service) <= 0 {
		return
	}
	p.genImportCode(file)
}
