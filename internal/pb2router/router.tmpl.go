/*
@Author: nullzz
@Date: 2020/9/30 4:41 下午
@Version: 1.0
*/
package pb2router

var tmplService = `

{{- range $sName, $serviceConfig := .Services }}
	{{- range $index, $methodConfig := $serviceConfig.Methods }}
			type {{$methodConfig.MethodName}}Handler struct{}
			func (*{{$methodConfig.MethodName}}Handler) {{$methodConfig.MethodName}}(cxt *gin.Context, pbmsg *{{$methodConfig.InputTypeName}}) (*{{$methodConfig.OutputTypeName}}, error) {
				err, replyoctets := linkserver.RouteClientMsg(pbmsg, cxt)
				if err != nil {
					zaplog.LoggerSugar.Warnf("routeReqAndReply failed!! err=%s", err.Error())
					return nil, err2.AppErrorFromProtoEnum(netutils.ErrorCode_Unknown)
				}

				clientmsg := &msg.ClientMessage{}
				pbbytes, err := linkserver.ParseServiceReply(replyoctets)
				if err := proto.Unmarshal(pbbytes, clientmsg); err != nil {
					return nil, err2.AppErrorFromProtoEnum(netutils.ErrorCode_Unknown)
				}
					return clientmsg, nil
				}
	{{- end}}
{{- end}}

func Register{{.ServiceFileName}}RouterGroup(routerGroup *gin.RouterGroup){
	{{- range $sName, $serviceConfig := .Services }}
		{{- range $index, $methodConfig := $serviceConfig.Methods }}
		    _ = service2.Register{{$sName}}HttpHandler(routerGroup, &{{$methodConfig.MethodName}}Handler{}) 
		{{- end}}
	{{- end}}

}

`
