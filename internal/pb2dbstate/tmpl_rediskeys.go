/*
@Author: nullzz
@Date: 2020/9/30 4:41 下午
@Version: 1.0
*/
package pb2dbstate

var tmplRedisKeys = `
package rediskeys

//import "strconv"

const (
{{- range $i, $val := .RedisKeys }}
	RedisKeyPrefix{{$val}}="{{index $.RedisKeyVals $i}}:"
{{- end}}
)

{{- range $i, $val := .RedisKeys }}
{{if eq $val "Account"}}
func Get{{$val}}TableKey(acc string) string {
	return RedisKeyPrefix{{$val}} + acc 
}
{{else}}
func Get{{$val}}TableKey(uid string) string {
	return RedisKeyPrefix{{$val}} + uid
}
{{end}}


{{- end}}

`
