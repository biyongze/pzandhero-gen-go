/*
@Author: nullzz
@Date: 2020/12/12 5:44 下午
@Version: 1.0
@DEC:
*/
package pb2dbstate

var tmplCheckRedis = `
package checkredis

import (
	"server/pkg/db/checkredis"
	"server/pkg/gen/dbhelper/dao"
	"server/pkg/gen/dbstate"
	"server/pkg/gen/rediskeys"
	"server/pkg/gen/stateop"
	"server/pkg/handlerenv"
	"server/pkg/keywrapper"
	"server/pkg/module"
	"server/pkg/redis/optemplate"
)

func init(){
	checkredis.Register("{{.Name}}", &Check{{.Name}}Helper{})
}

type Check{{.Name}}Helper struct {
}

func (c *Check{{.Name}}Helper) RedisKey(kw *keywrapper.KeyWrapper) string {
	return rediskeys.Get{{.RedisKey}}TableKey(kw.Uid)
}

func (c *Check{{.Name}}Helper) RedisGet(lockid string) interface{} {
	return optemplate.GetTemplate().RedisDoGet(&stateop.{{.Name}}Get{}, lockid)
}

func (c *Check{{.Name}}Helper) RedisSet(kw *keywrapper.KeyWrapper, data interface{}) {
	d := data.(*dbstate.{{.Name}})
	optemplate.SetTemplate().RedisDoSet(&stateop.{{.Name}}Set{}, kw, d, false) //fromdb要用false，否则不存库
}

func (c *Check{{.Name}}Helper) LogKeyName() string {
	return "Check{{.Name}}"
}

func (c *Check{{.Name}}Helper) DBFind(lockid string) (interface{}, error) {
	return dao.Get{{.Name}}Mgr().Find(lockid)
}

func (c *Check{{.Name}}Helper) InitData(env *handlerenv.ClientHandlerEnv, serverid int32) error {
	m, err := module.GetModule("{{.Name}}")
	if err != nil {
		return err
	}
	m.InitTable(env, env.ToKeyWarpper(""))
	return m.NewAccInit(env, env.GetLockid(), serverid)
}
`
