/*
@Author: nullzz
@Date: 2020/9/30 4:41 下午
@Version: 1.0
*/
package pb2dbstate

var tmplDataExport = `
package export


import (
	"github.com/golang/protobuf/proto"
	"server/pkg/gen/dbhelper/dao"
	"server/pkg/gen/dbstate"
	"server/pkg/gen/rediskeys"
)

type {{.Name}}Export struct{}

func ({{.Name}}Export) DataExport(playeruid string) (interface{}, error) {
	return dao.Get{{.Name}}Mgr().Find(playeruid)
}

func ({{.Name}}Export) DataImport(playeruid string) (string, proto.Message) {
	return rediskeys.Get{{.RedisKey}}TableKey(playeruid), &dbstate.{{.Name}}{}
}

`

var tmplDataExportRegister = `
package export


import (
	"github.com/golang/protobuf/proto"
	"server/pkg/gen/rediskeys"
)

func init() {
{{ range .Items }}
	Register(rediskeys.RedisKeyPrefix{{.RedisKey}}, {{.Name}}Export{})
{{ end}}
}

type Exportor interface {
	//导出
	DataExport(playeruid string) (interface{}, error)
	//导入¬ return:rediskey,data
	DataImport(playeruid string) (string, proto.Message)
}

var exportprs = make(map[string]Exportor)

func Register(redisKey string, e Exportor) {
	exportprs[redisKey] = e
}

func GetExportprs() map[string]Exportor {
	return exportprs
}

`