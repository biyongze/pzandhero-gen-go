/*
@Author: nullzz
@Date: 2020/9/30 4:41 下午
@Version: 1.0
*/
package pb2dbstate

//{{- range $i, $val := .Names }}
//
//var RediskeyPrefix{{$val}}="{{index $.KeyVals $i}}:"

var tmpl = `
package stateop

{{if eq .Name "AccountState"}}
import (
	"bitbucket.org/funplus/golib/zaplog"
	"github.com/golang/protobuf/proto"
	"server/pkg/gen/dbhelper/dao"
	"server/pkg/gen/dbstate"
	"server/pkg/gen/rediskeys"
	"server/pkg/keywrapper"
	"server/pkg/redis"
	"server/pkg/db"
	"server/pkg/redis/optemplate"
	"errors"
)
func init(){
	db.GetDBUpdateMgr().Add(rediskeys.RedisKeyPrefixAccount, UpdateAccount2DB)
	redis.GetRedisInterfaceMgr().Register(rediskeys.RedisKeyPrefix{{.RedisKey}}, &{{.Name}}Get{}, &{{.Name}}Set{})
}
type AccountStateSet struct {
}

func (*AccountStateSet) GetTablePrefix() string {
	return rediskeys.RedisKeyPrefixAccount
}

func (*AccountStateSet) GetTableKey(kw *keywrapper.KeyWrapper) string {
	return rediskeys.GetAccountTableKey(kw.Acc)
}

func (*AccountStateSet) ToRealState(state interface{}) proto.Message {
	return state.(*dbstate.AccountState)
}

//get
type AccountStateGet struct {
}

func (*AccountStateGet) GetTableKey(kw *keywrapper.KeyWrapper) string {
	return rediskeys.GetAccountTableKey(kw.Acc)
}

func (*AccountStateGet) GetInitState() proto.Message {
	return &dbstate.AccountState{}
}

func (*AccountStateGet) GetStateFromDB(kw *keywrapper.KeyWrapper) (proto.Message, error) {
	accdata, err := dao.GetAccountStateMgr().Find(kw.Acc)
	if err != nil {
		//zaplog.LoggerSugar.Errorf("GetAccountdata uid=%s from db error:%s", kw.Acc, err.Error())
		return nil, err
	}
	return accdata, nil
}

/*
dbsaveproxy用于将redis数据落地使用的方法
*/
func UpdateAccount2DB(acc string) error {
	data1 := optemplate.GetTemplate().RedisDoGet(&AccountStateGet{}, acc)
	if data1 == nil {
		zaplog.LoggerSugar.Warnf("acc=%s UpdateAccount2DB redis data nil", acc)
		return errors.New("update data failed. Redis data is nil")
	}
	data := data1.(*dbstate.AccountState)

	return dao.GetAccountStateMgr().Update(data.Account, data)
}
{{else}}
import (
	"bitbucket.org/funplus/golib/zaplog"
	"errors"
	"github.com/golang/protobuf/proto"
	"server/pkg/db"
	"server/pkg/gen/dbhelper/dao"
	"server/pkg/gen/dbstate"
	"{{.RedisKeyPackage}}/rediskeys"
	"server/pkg/keywrapper"
	"server/pkg/redis"
	"server/pkg/redis/optemplate"
)

func init(){
	db.GetDBUpdateMgr().Add(rediskeys.RedisKeyPrefix{{.RedisKey}}, Update{{.RedisKey}}2DB)
	redis.GetRedisInterfaceMgr().Register(rediskeys.RedisKeyPrefix{{.RedisKey}}, &{{.Name}}Get{}, &{{.Name}}Set{})
}

type {{.Name}}Set struct {
}

func (*{{.Name}}Set) GetTablePrefix() string {
	return rediskeys.RedisKeyPrefix{{.RedisKey}}
}

func (*{{.Name}}Set) GetTableKey(kw *keywrapper.KeyWrapper) string {
	return rediskeys.Get{{.RedisKey}}TableKey(kw.Uid)
}

func (*{{.Name}}Set) ToRealState(state interface{}) proto.Message {
	return state.(*dbstate.{{.Name}})
}

//get
type {{.Name}}Get struct {
}

func (*{{.Name}}Get) GetTableKey(kw *keywrapper.KeyWrapper) string {
	return rediskeys.Get{{.RedisKey}}TableKey(kw.Uid)
}

func (*{{.Name}}Get) GetInitState() proto.Message {
	return &dbstate.{{.Name}}{}
}

func (*{{.Name}}Get) GetStateFromDB(kw *keywrapper.KeyWrapper) (proto.Message, error) {
	adata, err := dao.Get{{.Name}}Mgr().Find(kw.Uid)
	if err != nil {
		zaplog.LoggerSugar.Errorf("User %s Get{{.Name}} err=%s", kw.Uid, err.Error()) //此处之所以报Fatal是因为不可能既不存在于redis也不存在于db
		return nil, err
	}
	return adata, nil
}

func Update{{.RedisKey}}2DB(uid string) error {
	data1 := optemplate.GetTemplate().RedisDoGet(&{{.Name}}Get{}, uid)
	if data1 == nil {
		zaplog.LoggerSugar.Warnf("uid=%s Update{{.Name}}2DB redis data nil", uid)
		return errors.New("update data failed. Redis data is nil")
	}
	data := data1.(*dbstate.{{.Name}})
	return dao.Get{{.Name}}Mgr().Update(uid, data)
}	
{{end}}

`
