package pb2dbstate

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"pzandhero-gen-go/internal/common"
	"strings"
	"text/template"
)

type Parser struct {
	Package string
	//DBStateName []string
	States []*DBState
}

type DBState struct {
	Name        string
	RedisKey    string
	RedisKeyVal string
}

func ParserDir(dir string, out string, pbPkg string) error {
	files, err := ioutil.ReadDir(dir)
	common.CheckErr(err)
	toPath := filepath.Join(out)
	if !common.Exists(toPath) {
		os.MkdirAll(toPath, os.ModePerm)
	}
	p := &Parser{}
	for _, f := range files {
		if f.IsDir() {
			continue
		}
		filepath := filepath.Join(dir, f.Name())
		p.ParserFile(filepath, pbPkg)
	}
	genDbStateFile(p, toPath)
	genRedisKeysFile(p, toPath)
	genDataExport(p, toPath)
	genDataExportRegister(p, toPath)
	genCheckData(p, toPath)
	return nil
}

func (p *Parser) ParserFile(source string, pbPkg string) error {
	f, err := os.Open(source)
	if err != nil {
		fmt.Println("read file fail", err)
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		str := scanner.Text()
		trimS := strings.TrimSpace(str)
		//p.regPackage(trimS)
		p.Package = pbPkg
		p.regDBState(scanner, trimS)
	}
	if err := scanner.Err(); err != nil {
		// Handle the error
	}
	return nil
}

// 匹配proto的包文件夹
func (p *Parser) regPackage(text string) {
	if strings.HasPrefix(text, "option go_package") {
		star := strings.Index(text, "\"")
		end := strings.LastIndex(text, "\"")
		p.Package = filepath.Join(text[star+1 : end])
	}
}

func (p *Parser) regDBState(scanner *bufio.Scanner, text string) {
	IsEntry := false
	if strings.HasPrefix(text, "// @Entry") {
		IsEntry = true
	} else {
		return
	}
	if IsEntry {
		for scanner.Scan() {
			str := scanner.Text()
			trimS := strings.TrimSpace(str)
			if strings.HasPrefix(trimS, "message") && strings.HasSuffix(trimS, "{") {
				IsEntry = false
				fields := strings.Fields(trimS)
				name := fields[1]
				s := strings.TrimSuffix(name, "State")
				state := &DBState{
					Name:        name,
					RedisKey:    s,
					RedisKeyVal: strings.ToUpper(s),
				}
				p.States = append(p.States, state)
				return
			}
		}
	}
}

type genDbState struct {
	Name            string
	RedisKeyPackage string
	RedisKey        string
	RedisKeyVal     string
}

// 生成dbstateOp
func genDbStateFile(p *Parser, out string) error {
	for _, d := range p.States {
		gen := &genDbState{
		}
		gen.Name = d.Name
		gen.RedisKey = d.RedisKey
		gen.RedisKeyVal = d.RedisKeyVal
		gen.RedisKeyPackage = p.Package
		toPath := filepath.Join(out, "stateop")
		if !common.Exists(toPath) {
			os.MkdirAll(toPath, os.ModePerm)
		}
		toPath2 := filepath.Join(toPath, d.Name+"Op.gen.go")
		file, err := os.OpenFile(toPath2, os.O_CREATE|os.O_WRONLY, 0755)
		common.CheckErr(err)
		t := template.Must(template.New("").Parse(tmpl))
		err = t.Execute(file, gen)
		common.CheckErr(err)
	}
	return nil
}

type genRedisKey struct {
	//RedisKeyPackage string
	RedisKeys    []string
	RedisKeyVals []string
}

// 生成rediskeys
func genRedisKeysFile(p *Parser, out string) {
	toPath := filepath.Join(out, "rediskeys")
	if !common.Exists(toPath) {
		os.MkdirAll(toPath, os.ModePerm)
	}
	gen := &genRedisKey{}
	for _, v := range p.States {
		gen.RedisKeys = append(gen.RedisKeys, v.RedisKey)
		gen.RedisKeyVals = append(gen.RedisKeyVals, v.RedisKeyVal)
	}
	to := filepath.Join(toPath, "rediskeys.gen.go")
	file, err := os.OpenFile(to, os.O_CREATE|os.O_WRONLY, 0755)
	common.CheckErr(err)
	t := template.Must(template.New("").Parse(tmplRedisKeys))
	err = t.Execute(file, gen)
	common.CheckErr(err)
}

type dataExport struct {
	Name     string
	RedisKey string
}

// 生成导出方法
func genDataExport(p *Parser, out string) {
	toPath := filepath.Join(out, "export")
	if !common.Exists(toPath) {
		os.MkdirAll(toPath, os.ModePerm)
	}
	for _, d := range p.States {
		if d.Name == "AccountState" {
			continue
		}
		gen := &dataExport{
		}
		gen.Name = d.Name
		gen.RedisKey = d.RedisKey
		toPath2 := filepath.Join(toPath, d.Name+"Export.gen.go")
		file, err := os.OpenFile(toPath2, os.O_CREATE|os.O_WRONLY, 0755)
		common.CheckErr(err)
		t := template.Must(template.New("").Parse(tmplDataExport))
		err = t.Execute(file, gen)
		common.CheckErr(err)
	}
}

type DataExportsGen struct {
	Items []DataExportRegister
}
type DataExportRegister struct {
	Name     string
	RedisKey string
}

// 生成导出方法
func genDataExportRegister(p *Parser, out string) {
	toPath := filepath.Join(out, "export")
	if !common.Exists(toPath) {
		os.MkdirAll(toPath, os.ModePerm)
	}

	gen := &DataExportsGen{}
	//var gen []*DataExportRegister

	for _, d := range p.States {
		if d.Name == "AccountState" {
			continue
		}
		de := DataExportRegister{
		}
		de.Name = d.Name
		de.RedisKey = d.RedisKey
		gen.Items = append(gen.Items, de)
		//gen = append(gen, de)

	}

	toPath2 := filepath.Join(toPath, "ExportRegister.gen.go")
	file, err := os.OpenFile(toPath2, os.O_CREATE|os.O_WRONLY, 0755)
	common.CheckErr(err)
	t := template.Must(template.New("").Parse(tmplDataExportRegister))
	err = t.Execute(file, gen)
	common.CheckErr(err)
}

type DataCheckModel struct {
	Name     string
	RedisKey string
}

// 生成用户数据检测
func genCheckData(p *Parser, out string) {
	for _, d := range p.States {
		if d.Name == "AccountState" {
			continue
		}
		if d.Name == "UserInfoState" {
			continue
		}
		gen := &DataCheckModel{
		}
		gen.Name = d.Name
		gen.RedisKey = d.RedisKey
		toPath := filepath.Join(out, "checkredis")
		if !common.Exists(toPath) {
			os.MkdirAll(toPath, os.ModePerm)
		}
		toPath2 := filepath.Join(toPath, "Check"+d.Name+"Helper.gen.go")
		file, err := os.OpenFile(toPath2, os.O_CREATE|os.O_WRONLY, 0755)
		common.CheckErr(err)
		t := template.Must(template.New("").Parse(tmplCheckRedis))
		err = t.Execute(file, gen)
		common.CheckErr(err)
	}
}
