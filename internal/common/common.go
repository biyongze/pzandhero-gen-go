package common

import (
	"os"
	"strings"
)

//判断是不是注释
func CheckAnnotation(text string) bool {
	s := strings.TrimSpace(text)
	return strings.HasPrefix(s, "//")
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

// 判断所给路径文件/文件夹是否存在
func Exists(path string) bool {
	_, err := os.Stat(path) //os.Stat获取文件信息
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		return false
	}
	return true
}
